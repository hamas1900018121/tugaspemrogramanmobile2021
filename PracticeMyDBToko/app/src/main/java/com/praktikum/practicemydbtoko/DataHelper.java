package com.praktikum.practicemydbtoko;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "PracticeDB.db";
    private static final int DATABASE_VERSION = 1;
    private SQLiteDatabase db;
    public DataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE Toko (IDToko VARCHAR(255) PRIMARY KEY, Nama_toko TEXT NULL, No_Telp CHAR(12))";
        Log.d("Data", "OnCreate: " + query);
        db.execSQL(query);

        query = "INSERT INTO Toko (IDToko, Nama_toko, No_Telp) VALUES ('SW1', 'Indomaret', '027445861354')";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }
}
