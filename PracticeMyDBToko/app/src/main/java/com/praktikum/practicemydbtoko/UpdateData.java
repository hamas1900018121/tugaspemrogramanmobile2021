package com.praktikum.practicemydbtoko;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UpdateData extends AppCompatActivity {
    protected Cursor cursor;
    private DataHelper dbHelper;
    private Button btn1, btn2;
    private EditText text1, text2, text3;
    private String IDToko;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_data);

        dbHelper = new DataHelper(this);
        text1 = (EditText) findViewById(R.id.inputIdToko);
        text2 = (EditText) findViewById(R.id.inputNamaToko);
        text3 = (EditText) findViewById(R.id.inputNoTelp);
        btn1 = (Button) findViewById(R.id.addData);
        btn2 = (Button) findViewById(R.id.showData);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM Toko WHERE Nama_toko = '" + getIntent().getStringExtra("nama") + "'",null); cursor.moveToFirst();
        if (cursor.getCount()>0) {
            cursor.moveToPosition(0);
            IDToko = cursor.getString(0).toString();
            text1.setText(cursor.getString(0).toString());
            text2.setText(cursor.getString(1).toString());
            text3.setText(cursor.getString(2).toString());
        }

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("UPDATE Toko SET IDtoko='" +
                        text1.getText().toString() +"', Nama_toko ='"+
                        text2.getText().toString() +"', No_Telp ='" +
                        text3.getText().toString() + "' WHERE IDToko = '" + IDToko + "'");
                Toast.makeText(getApplicationContext(), "Berhasil Update Data", Toast.LENGTH_LONG).show();
                MainActivity.ma.CreateData();
                finish();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UpdateData.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}