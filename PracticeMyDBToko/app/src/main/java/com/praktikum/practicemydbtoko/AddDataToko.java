package com.praktikum.practicemydbtoko;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddDataToko extends AppCompatActivity {
    DataHelper dbHelper;
    private EditText inputID;
    private EditText inputNama;
    private EditText inputNoTlp;
    private Button tambahData;
    private Button showData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data_toko);
        dbHelper = new DataHelper(this);
        inputID = (EditText) findViewById(R.id.inputIdToko);
        inputNama = (EditText) findViewById(R.id.inputNamaToko);
        inputNoTlp = (EditText) findViewById(R.id.inputNoTelp);

        tambahData = (Button) findViewById(R.id.addData);
        showData = (Button) findViewById(R.id.showData);

        tambahData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("INSERT INTO Toko (IDToko, Nama_toko, No_Telp) VALUES ('" +
                        inputID.getText().toString() +"','" +
                        inputNama.getText().toString() +"','" +
                        inputNoTlp.getText().toString() + "')");
                Toast.makeText(getApplicationContext(), "Berhasil Tambah Data", Toast.LENGTH_LONG).show();
                MainActivity.ma.CreateData();
                finish();
            }
        });

        showData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}